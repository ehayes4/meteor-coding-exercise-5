/*****************************************************************************/
/* Game: Event Handlers and Helpersss .js*/
/*****************************************************************************/
Template.Game.events ({
  'click #startGameButton': function (event, template) {
    event.preventDefault ();

    var player = {};
    player.userId = Meteor.user ()._id;
    var id = Player.insert (player);
    Session.set ('CURRENT_PLAYER_ID', id);
    console.log ('Adding Player: ' + Session.get ('CURRENT_PLAYER_ID'));
    Template.Game.phaser.createGame ();
  },
  'click #endGameButton': function (event, template) {
    event.preventDefault ();

    console.log ('Removing Player: ' + Session.get ('CURRENT_PLAYER_ID'));
    Player.remove ({_id: Session.get ('CURRENT_PLAYER_ID')});
    Session.set ('CURRENT_PLAYER_ID', null);
    Template.Game.phaser.tearDownGame ();
    $ ("#gameDiv").empty ();
  },
  'click #resetGameButton': function (event, template) {
    event.preventDefault ();

    //console.log ('Removing Player: ' + Session.get ('CURRENT_PLAYER_ID'));
    Player.find({}).fetch().forEach(function(player){
      Player.remove({_id:player._id});
    });
    Session.set ('CURRENT_PLAYER_ID', null);
    Template.Game.phaser.tearDownGame ();
    $ ("#gameDiv").empty ();
  }
});

Template.Game.helpers ({
  /*
   * Example:
   *  items: function () {
   *    return Items.find();
   *  }
   */
  players: function () {

    var cursor = Player.find ({_id: {$ne: Session.get ('CURRENT_PLAYER_ID')}});

    cursor.observe ({
      added: function (player) {
        console.log ('added player: ' + player._id);

        if (Template.Game.phaser.game && Template.Game.phaser.game.isBooted) {
          Template.Game.phaser.addPlayer (player._id);
        }


      },
      changed: function (newPlayer, oldPlayer) {
        // Not used...
        console.log ('changed player: ' + newPlayer._id);
      },
      removed: function (removedPlayer) {
        console.log ('removed player: ' + removedPlayer._id);

        if (Template.Game.phaser.game && Template.Game.phaser.game.isBooted) {
          Template.Game.phaser.removePlayer (removedPlayer._id);
        }

      }
    });

    return cursor;
  },
  currentPlayer: function () {

    console.log ('Current Player: ' + Session.get ('CURRENT_PLAYER_ID'));
    return Session.get ('CURRENT_PLAYER_ID');
  }
});

// PHASER GAME LIFECYCLE

Template.Game.phaser = {};

Template.Game.phaser.resetGame = function () {

  Template.Game.phaser.game = null;
  Template.Game.phaser.cursors = {};
  Template.Game.phaser.map = {};
  Template.Game.phaser.layer = {};
  Template.Game.phaser.players = {};
  Template.Game.phaser.jumpTimer = 0;
  Template.Game.phaser.jumpButton = {};
  Template.Game.phaser.bg = {};

};

Template.Game.phaser.createGame = function () {

  Template.Game.phaser.resetGame ();

  var width = 800;
  var height = 600;

  if (Meteor.isCordova) {
    width = 320;
    height = 568;
  }

  Template.Game.phaser.game = new Phaser.Game (width, height, Phaser.AUTO, 'gameDiv', {
    preload: preload,
    create: create,
    update: Template.Game.phaser.updateGame,
    render: render
  });

  console.log ('Instantiated Game.');

  function preload () {

    //console.log ('Meteor.absoluteUrl() ' + Meteor.absoluteUrl ())
    Template.Game.phaser.game.load.tilemap ('level1', 'assets/games/starstruck/level1.json', null, Phaser.Tilemap.TILED_JSON);
    Template.Game.phaser.game.load.image ('tiles-1', 'assets/games/starstruck/tiles-1.png');
    Template.Game.phaser.game.load.spritesheet ('dude', 'assets/games/starstruck/dude.png', 32, 48);
    Template.Game.phaser.game.load.spritesheet ('droid', 'assets/games/starstruck/droid.png', 32, 32);
    Template.Game.phaser.game.load.image ('starSmall', 'assets/games/starstruck/star.png');
    Template.Game.phaser.game.load.image ('starBig', 'assets/games/starstruck/star2.png');
    Template.Game.phaser.game.load.image ('background', 'assets/games/starstruck/background2.png');

    console.log ('Preload Complete.');
  }

  function create () {

    Template.Game.phaser.game.physics.startSystem (Phaser.Physics.ARCADE);

    Template.Game.phaser.game.stage.backgroundColor = '#000000';

    Template.Game.phaser.bg = Template.Game.phaser.game.add.tileSprite (0, 0, 800, 600, 'background');
    Template.Game.phaser.bg.fixedToCamera = true;

    Template.Game.phaser.map = Template.Game.phaser.game.add.tilemap ('level1');

    Template.Game.phaser.map.addTilesetImage ('tiles-1');

    Template.Game.phaser.map.setCollisionByExclusion ([13, 14, 15, 16, 46, 47, 48, 49, 50, 51]);

    Template.Game.phaser.layer = Template.Game.phaser.map.createLayer ('Tile Layer 1');

    //  Un-comment this on to see the collision tiles
    // layer.debug = true;

    Template.Game.phaser.layer.resizeWorld ();

    Template.Game.phaser.game.physics.arcade.gravity.y = 250;

    /*
     player = game.add.sprite (32, 32, 'dude');
     game.physics.enable (player, Phaser.Physics.ARCADE);

     player.body.bounce.y = 0.2;
     player.body.collideWorldBounds = true;
     player.body.setSize (20, 32, 5, 16);

     player.animations.add ('left', [0, 1, 2, 3], 10, true);
     player.animations.add ('turn', [4], 20, true);
     player.animations.add ('right', [5, 6, 7, 8], 10, true);
     */

    Template.Game.phaser.cursors = Template.Game.phaser.game.input.keyboard.createCursorKeys ();
    Template.Game.phaser.jumpButton = Template.Game.phaser.game.input.keyboard.addKey (Phaser.Keyboard.SPACEBAR);

    Template.Game.playerSubscription = Meteor.subscribe ('players', Meteor.user ()._id);

    var remotePlayers = Player.find ({_id: {$ne: Session.get ('CURRENT_PLAYER_ID')}}).fetch ();

    console.log ('Adding local player');
    Template.Game.phaser.addPlayer (Session.get ('CURRENT_PLAYER_ID'));
    //Template.Game.phaser.localPlayer = Template.Game.phaser.generatePlayer (Session.get ('CURRENT_PLAYER_ID'));
    Template.Game.phaser.game.camera.follow (Template.Game.phaser.localPlayer ());

    remotePlayers.forEach (function (remotePlayer) {

      console.log ('Adding remote player: ' + remotePlayer._id);
      Template.Game.phaser.addPlayer (remotePlayer._id);
      var player = Template.Game.phaser.playerForId (remotePlayer._id);

    });

    PlayerStream.on ('playerMovement', function (movement) {
      //Template.Game.phaser.updatePlayerMovement(movement);

      if (movement.playerId != undefined && movement.playerId != Session.get ('CURRENT_PLAYER_ID')) {

        //console.log ("PlayerStream.on('updatePlayerMovement' " + JSON.stringify (movement) + ')');

        Template.Game.phaser.updatePlayerMovement (movement);

      }

    });


    // Populate remote players:


    console.log ('Create Complete.');

  }

  function render () {


    // game.debug.text(game.time.physicsElapsed, 32, 32);
    // game.debug.body(player);
    // game.debug.bodyInfo(player, 16, 24);

  }


}

Template.Game.phaser.tearDownGame = function () {

  Template.Game.playerSubscription.stop ();
  Template.Game.phaser.game.destroy ();
  Template.Game.phaser.resetGame ();
  PlayerStream.on ('playerMovement', null);
}

// COMMON PLAYER

Template.Game.phaser.generatePlayer = function (playerId) {
  // TODO, make this player's position random...
  var newPlayer = Template.Game.phaser.game.add.sprite (32, 32, 'dude');
  Template.Game.phaser.game.physics.enable (newPlayer, Phaser.Physics.ARCADE);

  newPlayer.body.bounce.y = 0.2;
  newPlayer.body.collideWorldBounds = true;
  newPlayer.body.setSize (20, 32, 5, 16);

  newPlayer.animations.add ('left', [0, 1, 2, 3], 10, true);
  newPlayer.animations.add ('turn', [4], 20, true);
  newPlayer.animations.add ('right', [5, 6, 7, 8], 10, true);

  newPlayer.playerId = playerId;
  Template.Game.phaser.resetPlayerMovement (newPlayer);
  newPlayer.movement.facing = 'left';

  return newPlayer;

}

Template.Game.phaser.updateGame = function () {

  //console.log ('Game Loop');
  //Template.Game.phaser.updateLocalPlayer ();
  if (Template.Game.phaser.localPlayer ()) {
    Template.Game.phaser.updateLocalPlayerPositionWithKeyboard ();
    Template.Game.phaser.publishLocalPlayerMovement (Template.Game.phaser.localPlayer ().movement);
  }
  Template.Game.phaser.updateAllRemotePlayers ();
  return Template.Game.phaser.game;
}

Template.Game.phaser.resetPlayerMovement = function (aPlayer) {
  aPlayer.movement = {};
  aPlayer.movement.left = false;
  aPlayer.movement.right = false;
  aPlayer.movement.jump = false;

  aPlayer.movement.playerId = aPlayer.playerId;
}

Template.Game.phaser.updatePlayer = function (aPlayer) {

  //console.log ('Updating Player: ' + JSON.stringify (aPlayer.movement));

  Template.Game.phaser.game.physics.arcade.collide (aPlayer, Template.Game.phaser.layer);

  aPlayer.body.velocity.x = 0;

  if (aPlayer.movement.left == true) {

    //console.log ('Player moving Left');

    aPlayer.body.velocity.x = -150;

    if (aPlayer.movement.facing != 'left') {
      aPlayer.animations.play ('left');
      aPlayer.movement.facing = 'left';
      //console.log ('Turning player Left');
    }
  }
  else if (aPlayer.movement.right == true) {

    //console.log ('Player moving Right');

    aPlayer.body.velocity.x = 150;

    if (aPlayer.movement.facing != 'right') {
      aPlayer.animations.play ('right');
      aPlayer.movement.facing = 'right';

      //console.log ('Turning player Right');
    }
  }
  else {

    //console.log ('Player not moving');

    if (aPlayer.movement.facing != 'idle') {

      //console.log ('Stopping Player');

      aPlayer.animations.stop ();

      if (aPlayer.movement.facing == 'left') {
        aPlayer.frame = 0;
      }
      else {
        aPlayer.frame = 5;
      }

      aPlayer.movement.facing = 'idle';
    }
  }

  if (aPlayer.movement.jump && aPlayer.body.onFloor () && Template.Game.phaser.game.time.now > Template.Game.phaser.jumpTimer) {
    //console.log ('Jumping Player');
    aPlayer.body.velocity.y = -250;
    Template.Game.phaser.jumpTimer = Template.Game.phaser.game.time.now + 1500;
  }

  aPlayer.movement.x = aPlayer.x;
  aPlayer.movement.y = aPlayer.y;

}

// LOCAL PLAYER

Template.Game.phaser.localPlayer = function () {

  return Template.Game.phaser.playerForId (Session.get ('CURRENT_PLAYER_ID'));

}

/*
 Template.Game.phaser.updateLocalPlayer =  function () {

 if (Template.Game.phaser.localPlayer()) {
 Template.Game.phaser.updateLocalPlayerMovementWithKeyboard (Template.Game.phaser.localPlayer());
 Template.Game.phaser.updatePlayer (Template.Game.phaser.localPlayer());
 Template.Game.phaser.publishLocalPlayerMovement(Template.Game.phaser.localPlayer().movement);
 }

 }
 */

Template.Game.phaser.updateLocalPlayerPositionWithKeyboard = function () {

  var aPlayer = Template.Game.phaser.localPlayer ();

  Template.Game.phaser.updateLocalPlayerMovementWithKeyboard (aPlayer);

  Template.Game.phaser.updatePlayer (aPlayer);


}

Template.Game.phaser.updateLocalPlayerMovementWithKeyboard = function (aPlayer) {

  Template.Game.phaser.resetPlayerMovement (aPlayer);

  if (Template.Game.phaser.cursors.left.isDown) {
    aPlayer.movement.left = true;

  }
  else if (Template.Game.phaser.cursors.right.isDown) {
    aPlayer.movement.right = true;
  }

  if (Template.Game.phaser.jumpButton.isDown) {

    aPlayer.movement.jump = true;

  }

  return aPlayer;
}

Template.Game.phaser.publishLocalPlayerMovement = function (movement) {
  PlayerStream.emit ('playerMovement', movement);
}

// REMOTE PLAYER

Template.Game.phaser.addPlayer = function (playerId) {
  var aPlayer = Template.Game.phaser.generatePlayer (playerId);
  Template.Game.phaser.players[aPlayer.playerId] = aPlayer;
}

Template.Game.phaser.removePlayer = function (playerId) {
  var playerToRemove = Template.Game.phaser.playerForId (playerId);


  /*
   Template.Game.phaser.players = _.reject (Template.Game.phaser.players, function (remotePlayer) {
   remotePlayer.playerId == playerId;
   });
   */
  playerToRemove.destroy ();
  delete Template.Game.phaser.players[playerId];

}

Template.Game.phaser.playerForId = function (playerId) {

  return Template.Game.phaser.players[playerId];
  //var remotePlayer = _.findWhere (Template.Game.phaser.players, {playerId: playerId});
  //return remotePlayer;
}

Template.Game.phaser.updatePlayerMovement = function (movement) {

  Template.Game.phaser.players[movement.playerId].movement = movement;

  /*
   Template.Game.phaser.players = _.map(Template.Game.phaser.players,function(remotePlayer){

   if (remotePlayer.playerId == movement.playerId) {
   //console.log('Updating remote player movevment.');
   remotePlayer.movement = movement;
   }

   return remotePlayer;

   });
   */
}

Template.Game.phaser.updateAllRemotePlayers = function () {

  var players = _.values (Template.Game.phaser.players).forEach (function (player) {
    //Template.Game.phaser.updateRemotePlayer (player);

    if (player.playerId != Session.get ('CURRENT_PLAYER_ID')) {

      //Template.Game.phaser.updatePlayer(player);
      Template.Game.phaser.updateRemotePlayerMovement (player);

    }

  });

  /*
   Template.Game.phaser.players.forEach (function (remotePlayer) {
   Template.Game.phaser.updateRemotePlayer (remotePlayer);
   })
   */
}

Template.Game.phaser.updateRemotePlayerMovement = function (remotePlayer) {

  remotePlayer.x = remotePlayer.movement.x;
  remotePlayer.y = remotePlayer.movement.y;
  Template.Game.phaser.updatePlayer (remotePlayer);

}
/*
 Template.Game.phaser.updateRemotePlayer = function (remotePlayer) {

 Template.Game.phaser.updatePlayer(remotePlayer);

 }
 */

/*****************************************************************************/
/* Game: Lifecycle Hooks */
/*****************************************************************************/
Template.Game.created = function () {


};

Template.Game.rendered = function () {


  Session.set ('CURRENT_PLAYER_ID', null);

};

Template.Game.destroyed = function () {

  //Template.Game.phaser.tearDownGame();
};