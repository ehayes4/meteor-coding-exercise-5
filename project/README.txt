
Player collection. When users come online or go offline, a record is added to this collection.
User Status: https://atmospherejs.com/mizzao/user-status
See: server/userStatus.js

Meteor Streams for realtime communication: http://arunoda.github.io/meteor-streams/

Publish: server/publish/players.js
Subscribe: client/view/game/game.js
Good article explaining meteor publish.subscribe: https://www.discovermeteor.com/blog/understanding-meteor-publications-and-subscriptions/

Phaser Docs: http://docs.phaser.io/